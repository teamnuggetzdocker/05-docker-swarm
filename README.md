# Docker Swarm Demo Readme

Steps to run the demo

- Run this command

	docker-machine create manager1
	
	docker-machine start manager1
	
- To allow insecure http connections to the registry

	docker-machine ssh manager1
	
	sudo vi /mnt/sda1/var/lib/boot2docker/profile
	
Add the --insecure-registry line as below

	--label provider=virtualbox
	--insecure-registry 192.168.99.100:5000

Save the file and exit the ssh connection

	exit
	
	docker-machine restart manager1
	
- Repeat the above steps creating two more machines named worker1 and worker2

- Create a swarm (use the IP given to manager1, in my case its 192.168.99.101)

	docker-machine ssh manager1

	docker swarm init --advertise-addr 192.168.99.101
	
	docker info
	
	docker node ls
	
	exit
	
- Add a worker to the swarm (use your command output in the terminal when swarm initialized)

	docker-machine ssh worker1
	
	docker swarm join --token SWMTKN-1-2rmhm0g4211rj7kuoemhg0du5kcyco17k6klpq6p8khsn8zzqi-7y0v08albq1top63vczhdwzau 192.168.99.101:2377
	
	exit
	
- Repeat above for worker2

- Create an overlay network

	docker network create --driver overlay nttdatalab-overlay

- Deploy the nuggetzbanktransactions service

	docker-machine ssh manager1
	
	docker service create --replicas 1 --name nuggetzbanktransactions --network nttdatalab-overlay 192.168.99.100:5000/nttdatalab/nuggetzbank-transactions:1
	
	docker service ls
	
	docker service inspect nuggetzbanktransactions
	
	docker service ps nuggetzbanktransactions

- Scale the service
	
	docker service scale nuggetzbanktransactions=3
	
	docker service ps nuggetzbanktransactions
	
- Deploy the nuggetzbankaccounts service

	docker service create --replicas 3 --name nuggetzbankaccounts --network nttdatalab-overlay 192.168.99.100:5000/nttdatalab/nuggetzbank-accounts:1
	
	docker service ls
	
	docker service ps nuggetzbankaccounts
	
- Deploy the haproxy-project-specific service

	docker service create -p 8080:8080 --replicas 1 --name haproxyprojectspecific --network nttdatalab-overlay 192.168.99.100:5000/nttdatalab/haproxy-project-specific:2
	
	docker service ls
	
	docker service ps nuggetzbankaccounts

	
- Run this command against any of the swarm members 192.168.99.101, 192.168.99.102, 192.168.99.103

	curl -i http://192.168.99.101:8080/nuggetzbank-accounts/resources/accounts
	
You should see a response similar to:

	HTTP/1.1 200 OK
	Connection: close
	X-Powered-By: Undertow/1
	Server: WildFly/10
	Content-Type: application/octet-stream
	Content-Length: 48
	an account response plus: a transaction response
	